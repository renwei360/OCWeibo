//
//  RWUser.m
//  COWeibo
//
//  Created by as888 on 15/12/21.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "RWUser.h"

@implementation RWUser

+ (instancetype)userWithDict:(NSDictionary *)dict {

    id obj = [[self alloc] init];
    
    [obj setValuesForKeysWithDictionary:dict];
    
    return obj;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {

}

- (NSString *)description {

    NSArray *keys = @[@"id", @"screen_name", @"profile_image_url", @"verified_type", @"mbrank"];
    
    return [self dictionaryWithValuesForKeys:keys].description;
}
@end
