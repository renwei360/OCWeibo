//
//  RWUserAccont.m
//  COWeibo
//
//  Created by as888 on 15/12/20.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "RWUserAccont.h"


@implementation RWUserAccont

#pragma mark - 构造函数
+ (instancetype)userAccountWithDict:(NSDictionary *)dict {

    id obj = [[self alloc] init];
    
    [obj setValuesForKeysWithDictionary:dict];
    
    return obj;
}

// 对于数据模型中缺少的、不能与任何键配对的属性的时候，系统会自动调用setValue:forUndefinedKey:这个方法
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {

}

/// 对象的描述信息
- (NSString *)description {

   NSArray *keys = @[@"access_token", @"expires_in", @"uid", @"screen_name", @"avatar_large", @"expiresDate"];

    return [self dictionaryWithValuesForKeys:keys].description;
}

#pragma mark - 重写属性的方法
- (void)setExpires_in:(NSTimeInterval)expires_in {

    _expires_in = expires_in;
    
    self.expiresDate = [NSDate dateWithTimeIntervalSinceNow:expires_in];
}
@end
