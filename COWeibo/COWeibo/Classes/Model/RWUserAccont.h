//
//  RWUserAccont.h
//  COWeibo
//
//  Created by as888 on 15/12/20.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RWUserAccont : NSObject

/// 用于调用access_token，接口获取授权后的access token
@property (nonatomic, copy) NSString *access_token;
/// 提示：OC 中使用 NSString 也可以，Swift 中必须使用 NSTimeInterval
@property (nonatomic, assign) NSTimeInterval expires_in;
/// access token 的过期日期，由设置 expires_in 时设置
@property (nonatomic, strong) NSDate *expiresDate;
/// 当前授权用户的UID
@property (nonatomic, copy) NSString *uid;
/// 用户昵称
@property (nonatomic, copy) NSString *screen_name;
/// 用户头像
@property (nonatomic, copy) NSString *avatar_large;

+ (instancetype)userAccountWithDict:(NSDictionary *)dict;

@end
