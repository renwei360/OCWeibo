//
//  DiscoverSearchView.m
//  COWeibo
//
//  Created by as888 on 15/12/15.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "DiscoverSearchView.h"

@interface DiscoverSearchView()<UITextFieldDelegate>
/// 输入框
@property (nonatomic, strong) UITextField *textField;
/// 取消按钮
@property (nonatomic, strong) UIButton *cancelButton;

@end

@implementation DiscoverSearchView

- (instancetype)initWithFrame:(CGRect)frame {

    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

#pragma mark - 监听方法
- (void)clickCancelButton {
    [self.textField resignFirstResponder];
    
    [self updateTextFieldConstraint:0];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self updateTextFieldConstraint:self.cancelButton.bounds.size.width];
}

/// 动画更新文本框右侧约束
///
/// @param constraint 约束数值
- (void)updateTextFieldConstraint:(CGFloat)constraint {
    [self.textField mas_updateConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, constraint));
    }];
    
    [UIView animateWithDuration:0.25 animations:^{
        [self layoutIfNeeded];
    }];
}

#pragma mark - 更新ui
- (void)setupUI {
    
    // 添加控件
    [self addSubview:self.textField];
    [self addSubview:self.cancelButton];
    
    // 自动布局
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.bottom.equalTo(self);
        make.right.equalTo(self);
        make.width.mas_equalTo(45);
    }];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];

}

#pragma mark - 懒加载控件
- (UITextField *)textField {

    if (_textField == nil) {
        _textField = [[UITextField alloc] init];
        _textField.backgroundColor = [UIColor colorWithRed:0.4 green:.8 blue:1.0 alpha:0.5];
        _textField.placeholder = @"搜索按钮，view";
        _textField.layer.borderColor = [UIColor darkGrayColor].CGColor;
        _textField.layer.borderWidth = 3.0;
        _textField.layer.cornerRadius = 5.0;

        // 设置左侧视图
        UIImageView *leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchbar_textfield_search_icon"]];
        leftView.frame = CGRectMake(0, 0, self.bounds.size.height, self.bounds.size.height);
        leftView.contentMode = UIViewContentModeCenter;
        
        _textField.leftView = leftView;
        _textField.leftViewMode = UITextFieldViewModeAlways;
        
        _textField.delegate = self;
    }
    return _textField;
}

- (UIButton *)cancelButton {

    if (_cancelButton == nil) {
        _cancelButton = [[UIButton alloc] init];
        
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        
        [self addSubview:_cancelButton];
        
        // 监听方法
        [_cancelButton addTarget:self action:@selector(clickCancelButton) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}

@end
