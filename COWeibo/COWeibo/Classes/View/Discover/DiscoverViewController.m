//
//  DiscoverViewController.m
//  COWeibo
//
//  Created by as888 on 15/12/15.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "DiscoverViewController.h"
#import "DiscoverSearchView.h"
#import <objc/runtime.h>

@interface DiscoverViewController ()

@property (nonatomic, strong) DiscoverSearchView *searchView;

@end

@implementation DiscoverViewController

static UIView *_findView = nil;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUIWithImageName:@"visitordiscover_image_message" message:@"登录后，最新、最热微博尽在掌握，不再会与实事潮流擦肩而过" logonSuccessedBlock:^{
        // 设置搜索按钮
        self.searchView = [[DiscoverSearchView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen ff_screenSize].width, 35)];        self.navigationItem.titleView = self.searchView;
    
        self.tabBarItem.badgeValue = @"99";
        
        [self setupBadgeBackground];
    }];
}

/// 设置 Badge 数字的背景图片
- (void)setupBadgeBackground {
    
    // 查找 _UIBadgeView
    UIView *badgeView = [UIView ff_firstInView:self.tabBarController.tabBar clazzName:@"_UIBadgeBackground"];
    
    NSLog(@"最终结果 %@", badgeView);
    
    // 测试内部成员变量列表
    [badgeView ff_ivarsList];
    
    // 设置背景图片
    [badgeView setValue:[UIImage imageNamed:@"main_badge"] forKey:@"_image"];
}

@end
