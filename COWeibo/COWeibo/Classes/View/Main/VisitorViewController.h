//
//  VisitorViewController.h
//  COWeibo
//
//  Created by as888 on 15/12/15.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VisitorView.h"

@interface VisitorViewController : UITableViewController

/// 设置UI, 由子实现
///
/// @param imageName           访客视图图像名，首页传入 nil
/// @param message             访客视图提示信息
/// @param logonSuccessedBlock 登录成功后的 UI 设置 block
- (void)setupUIWithImageName:(NSString *)imageName message:(NSString *)message logonSuccessedBlock:(void(^)())logonSuccessedBlock;

@end
