//
//  MainViewController.m
//  COWeibo
//
//  Created by as888 on 15/12/15.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "MainViewController.h"
#import "NavigationController.h"
#import "HomeViewController.h"
#import "MessageViewController.h"
#import "DiscoverViewController.h"
#import "ProfileViewController.h"
#import "RWComposeTypeView.h"
#import "RWUserAccountViewModel.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface MainViewController ()

@property (nonatomic, strong) UIButton *composeButton;

@end

@implementation MainViewController

#pragma mark - 视图的生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self addChildViewControllers];
    
    // 隐藏 tabBar 上方分隔线的做法 － 现在有些应用程序的自定义 TabBar 会遇到，分享一下 :D
    self.tabBar.shadowImage = [[UIImage alloc] init];
    self.tabBar.backgroundImage = [UIImage imageNamed:@"tabbar_background"];
}

- (void)viewWillLayoutSubviews {

    [super viewWillLayoutSubviews];
    
    // 添加撰写按钮
    [self setupComposeButton];
}

#pragma mark - 监听方法
/// 点击撰写按钮
- (void)clickComposeButton {
    // 判断用户是否登录
    if (![RWUserAccountViewModel sharedUserAccount].isLogon) {
        [SVProgressHUD showInfoWithStatus:@"请先登录" maskType:SVProgressHUDMaskTypeGradient];
        
        return;
    }
    
    // 实例化撰写类型视图
    RWComposeTypeView *composeView = [[RWComposeTypeView alloc] initWithSelectedComposeType:^(RWComposeType *type) {
        
        DDLogInfo(@"选中撰写标题: %@ - 类名: %@", type.title, type.controllerName);
        
        if (type.controllerName != nil) {
            Class cls = NSClassFromString(type.controllerName);
            
            // 判断类名是否存在
            if (cls == nil) {
                return;
            }
            
            UIViewController *vc = [[cls alloc] init];
            
            UINavigationController *nav = [[NavigationController alloc] initWithRootViewController:vc];
            
            [self presentViewController:nav animated:YES completion:nil];
        }
    }];
    
    // 在当前视图中显示
    [composeView showInView:self.view];
}

#pragma mark - 设置ui
/// 设置撰写按钮的位置
- (void) setupComposeButton {
    
    CGRect rect = self.tabBar.bounds;
    CGFloat w = rect.size.width / self.childViewControllers.count - 1;
    
    self.composeButton.frame = CGRectInset(rect, 2 * w, 0);
}

#pragma mark - 设置子控制器
- (void)addChildViewControllers {
   
    // 设置 tabBar 的 tintColor
    self.tabBar.tintColor = [UIColor orangeColor];
    
    // 添加子控制器
    [self addChildViewController:[[HomeViewController alloc] init] title:@"首页" imageName:@"tabbar_home"];
    [self addChildViewController:[[MessageViewController alloc] init] title:@"消息" imageName:@"tabbar_message_center"];
    
    // 添加一个空白的追求
    [self addChildViewController:[[UIViewController alloc] init]];
    [self addChildViewController:[[DiscoverViewController alloc] init] title:@"发现" imageName:@"tabbar_discover"];
    [self addChildViewController:[[ProfileViewController alloc] init] title:@"我" imageName:@"tabbar_profile"];

}

/// 设置子控制器
- (void)addChildViewController:(UIViewController *)childController title: (NSString *)title imageName:(NSString *)imageName {

    childController.title = title;
    
    // 通过 AttributeText 设置字体属性
    // 设置字体颜色
    //[childController.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor redColor]} forState:UIControlStateHighlighted];
    // 设置字体大小
    // [childController.tabBarItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:20]} forState:UIControlStateNormal];
    
    childController.tabBarItem.image = [UIImage imageNamed:imageName];
    NSString *selectedImageName = [NSString stringWithFormat:@"%@_selected",imageName];
    childController.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    // 添加子控制器
    NavigationController *nav = [[NavigationController alloc]initWithRootViewController:childController];
    
    [self addChildViewController:nav];
}
#pragma mark - 懒加载控件
- (UIButton *)composeButton {
    if (_composeButton == nil) {
        _composeButton = [[UIButton alloc] init];
        
        // 设置按钮图像
        [_composeButton setImage:[UIImage imageNamed:@"tabbar_compose_icon_add"] forState:UIControlStateNormal];
        [_composeButton setImage:[UIImage imageNamed:@"tabbar_compose_icon_add_highlighted"] forState:UIControlStateHighlighted];
        [_composeButton setBackgroundImage:[UIImage imageNamed:@"tabbar_compose_button"] forState:UIControlStateNormal];
        [_composeButton setBackgroundImage:[UIImage imageNamed:@"tabbar_compose_button_highlighted"] forState:UIControlStateHighlighted];
        
        // 添加到 tabBar
        [self.tabBar addSubview:_composeButton];
        
        // 添加按钮监听方法
        [_composeButton addTarget:self action:@selector(clickComposeButton) forControlEvents:UIControlEventTouchUpInside];
    }
    return _composeButton;
}



@end
