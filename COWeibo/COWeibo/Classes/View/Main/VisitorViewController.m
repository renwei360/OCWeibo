//
//  VisitorViewController.m
//  COWeibo
//
//  Created by as888 on 15/12/15.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "VisitorViewController.h"
#import "VisitorView.h"
#import "OAuthViewController.h"
@interface VisitorViewController ()

/// 用户登录标记
@property (nonatomic, assign) BOOL userLogon;

/// 访客视图
@property (nonatomic, strong) VisitorView *visitorView;

@end

@implementation VisitorViewController

- (void)loadView {

    //self.userLogon = NO; //[RWUserAccountViewModel sharedUserAccount].isLogon;
    self.userLogon = [RWUserAccountViewModel sharedUserAccount].isLogon;
    self.userLogon ? [super loadView] : [self setupVisitorView];
}

#pragma mark - 监听了方法
/// 点击注册按钮
- (void)clickRegisterButton {

    NSLog(@"点击了注册按钮");
}
/// 点击了登录按钮
- (void)clickLoginButton {

    DDLogInfo(@"点击了用户登录");
    
    OAuthViewController *vc = [[OAuthViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - 设置界面
/// 设置访客视图
- (void)setupVisitorView {

    self.visitorView = [[VisitorView alloc] init];
    
    self.view = self.visitorView;
}

/// 设置界面
- (void)setupUIWithImageName:(NSString *)imageName message:(NSString *)message logonSuccessedBlock:(void (^)())logonSuccessedBlock {

    // 设置未登录的导航栏信息
    if (!self.userLogon) {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem ff_barButtonWithTitle:@"注销" imageName:nil target:self action:@selector(clickRegisterButton)];
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem ff_barButtonWithTitle:@"登录" imageName:nil target:self action:@selector(clickLoginButton)];
        
        // 设置访客视图
        [self.visitorView visitorInfoWithImageName:imageName message:message];
        
        // 添加按钮的点击方法
        [self.visitorView.registerButton addTarget:self action:@selector(clickRegisterButton) forControlEvents:UIControlEventTouchUpInside];
        [self.visitorView.loginButton addTarget:self action:@selector(clickLoginButton) forControlEvents:UIControlEventTouchUpInside];
    }else if (logonSuccessedBlock != nil) {
    
        logonSuccessedBlock();
    }
    
}
@end
