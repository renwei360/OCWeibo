//
//  VisitorView.h
//  COWeibo
//
//  Created by as888 on 15/12/15.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VisitorView : UIView
/// 注册按钮
@property (nonatomic, strong) UIButton *registerButton;
/// 登录按钮
@property (nonatomic, strong) UIButton *loginButton;

/// 设置访客视图信息
///
/// @param imageName 图像名称 - 首页图像传入 nil
/// @param title     信息文本
- (void)visitorInfoWithImageName:(NSString *)imageName message:(NSString *)message;
@end
