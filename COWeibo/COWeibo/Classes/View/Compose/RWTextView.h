//
//  RWTextView.h
//  COWeibo
//
//  Created by as888 on 15/12/27.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface RWTextView : UITextView

/// 占位文本属性
@property (nonatomic, copy) IBInspectable NSString *placeholder;

@end
