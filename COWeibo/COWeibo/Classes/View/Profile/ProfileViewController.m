//
//  ProfileViewController.m
//  COWeibo
//
//  Created by as888 on 15/12/15.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

#pragma mark - 设置界面
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setupUIWithImageName:@"visitordiscover_image_profile" message:@"登录后，你的微博、相册、个人资料会显示在这里，展示给别人" logonSuccessedBlock:^{
        self.tabBarItem.badgeValue = @"88";
    }];
}
@end
