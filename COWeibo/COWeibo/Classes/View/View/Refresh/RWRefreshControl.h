//
//  RWRefreshControl.h
//  COWeibo
//
//  Created by as888 on 15/12/25.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 下拉刷新控件
@interface RWRefreshControl : UIControl

/// 结束刷新
- (void)endRefreshing;

@end
