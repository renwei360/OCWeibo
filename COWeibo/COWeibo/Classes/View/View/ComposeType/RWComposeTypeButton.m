//
//  RWComposeTypeButton.m
//  COWeibo
//
//  Created by as888 on 15/12/26.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "RWComposeTypeButton.h"
#import "RWComposeType.h"

@implementation RWComposeTypeButton

- (void)setComposeType:(RWComposeType *)composeType {
    _composeType = composeType;
    
    [self setTitle:composeType.title forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:composeType.icon] forState:UIControlStateNormal];
}

#pragma mark - 构造函数

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        
        [self setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    return self;
}

/// 重写高亮属性
- (void)setHighlighted:(BOOL)highlighted {}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect imageRect = self.bounds;
    imageRect.size.height = imageRect.size.width;
    self.imageView.frame = imageRect;
    
    CGRect labelRect = self.bounds;
    labelRect.origin.y = labelRect.size.width;
    labelRect.size.height -= labelRect.size.width;
    self.titleLabel.frame = labelRect;
}

@end
