//
//  RWComposeTypeView.h
//  COWeibo
//
//  Created by as888 on 15/12/26.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWComposeType.h"

@interface RWComposeTypeView : UIView

/// 构造函数
///
/// @param completed 完成回调
///
/// @return 撰写类型视图
- (instancetype)initWithSelectedComposeType:(void (^)(RWComposeType *type))completed;

/// 显示在指定视图中
- (void)showInView:(UIView *)view;

@end
