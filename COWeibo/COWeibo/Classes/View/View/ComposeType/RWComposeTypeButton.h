//
//  RWComposeTypeButton.h
//  COWeibo
//
//  Created by as888 on 15/12/26.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RWComposeType;

@interface RWComposeTypeButton : UIButton

@property (nonatomic, strong) RWComposeType *composeType;

@end
