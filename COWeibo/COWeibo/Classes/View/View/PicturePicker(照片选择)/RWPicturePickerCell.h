//
//  RWPicturePickerCell.h
//  COWeibo
//
//  Created by as888 on 15/12/27.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RWPicturePickerCell;

@protocol RWPicturePickerCellDelegate<NSObject>

@optional
- (void)picturePickerCellDidClickDeleteButton:(RWPicturePickerCell *)cell;

@end

@interface RWPicturePickerCell : UICollectionViewCell

/// 照片图像
@property (nonatomic, strong) UIImage *image;
/// 代理
@property (nonatomic, weak) id<RWPicturePickerCellDelegate>delegate;

@end
