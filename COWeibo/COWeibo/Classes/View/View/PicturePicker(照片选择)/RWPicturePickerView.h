//
//  RWPicturePickerView.h
//  COWeibo
//
//  Created by as888 on 15/12/27.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RWPicturePickerView : UICollectionView

/// 照片数组
@property (nonatomic, strong) NSMutableArray *pictures;

/// 实例化视图，并指定添加照片回调
///
/// @param addImageCallBack 添加照片回调
- (instancetype)initWithAddImageCallBack:(void(^)())addImageCallBack;

/// 添加一张图片
- (void)addImage:(UIImage *)image;
@end
