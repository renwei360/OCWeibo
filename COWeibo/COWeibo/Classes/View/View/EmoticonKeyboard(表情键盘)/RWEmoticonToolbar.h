//
//  RWEmoticonToolbar.h
//  COWeibo
//
//  Created by as888 on 15/12/27.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 表情工具栏按钮类型
typedef enum : NSUInteger {
    EmoticonToolbarRecent,
    EmoticonToolbarNormal,
    EmoticonToolbarEmoji,
    EmoticonToolbarLangXiaohua,
} RWEmoticonToolbarType;

@protocol RWEmoticonToolbarDelegate <NSObject>

/// 工具栏选中表情类型
///
/// @param type 表情类型
- (void)emoticonToolbarDidSelectEmoticonType:(RWEmoticonToolbarType)type;

@end

/// 表情键盘工具条
@interface RWEmoticonToolbar : UIView

/// 代理
@property (nonatomic, weak) id<RWEmoticonToolbarDelegate> delegate;

/// 使用 section 设置选中按钮
- (void)setSelectedButtonWithSection:(NSInteger)section;

@end
