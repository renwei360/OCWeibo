//
//  RWEmoticonButton.h
//  COWeibo
//
//  Created by as888 on 15/12/28.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RWEmoticon;

/// 表情按钮
@interface RWEmoticonButton : UIButton

/// 表情包模型
@property (nonatomic, strong) RWEmoticon *emoticon;
/// 是否删除按钮
@property (nonatomic, assign) BOOL isDeleteButton;

@end
