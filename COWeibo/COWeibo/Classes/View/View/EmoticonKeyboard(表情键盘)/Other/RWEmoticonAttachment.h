//
//  HMEmoticonAttachment.h
//  COWeibo
//
//  Created by as888 on 15/12/28.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RWEmoticon;

/// 自定义支持表情符号的文本附件
@interface HMEmoticonAttachment : NSTextAttachment

/// 表情模型
@property (nonatomic, strong) RWEmoticon *emoticon;

/// 使用表情模型实例化附件
+ (instancetype)attachmentWithEmoticon:(RWEmoticon *)emoticon;

/// 使用指定字体生成属性文本
- (NSAttributedString *)imageTextWithFont:(UIFont *)font;
@end
