//
//  RWEmoticonTipView.h
//  COWeibo
//
//  Created by as888 on 15/12/28.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RWEmoticon;

/// 表情提示视图
@interface RWEmoticonTipView : UIImageView

/// 显示指定按钮中的内容
@property (nonatomic) RWEmoticon *emoticon;

@end
