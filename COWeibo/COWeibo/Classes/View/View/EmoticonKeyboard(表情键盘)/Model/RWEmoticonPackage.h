//
//  RWEmoticonPackage.h
//  COWeibo
//
//  Created by as888 on 15/12/27.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <Foundation/Foundation.h>

/// 表情包模型
@interface RWEmoticonPackage : NSObject

/// 表情包分组名
@property (nonatomic, copy) NSString *groupName;
/// 表情包中的表情模型数组
@property (nonatomic, strong) NSMutableArray *emoticonsList;

+ (instancetype)emoticonPackageWithDict:(NSDictionary *)dict;
- (instancetype)initWithDict:(NSDictionary *)dict;

@end
