//
//  RWEmoticonCell.h
//  COWeibo
//
//  Created by as888 on 15/12/27.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RWEmoticon;

@protocol RWEmoticonCellDelegate <NSObject>

@optional
/// 表情cell选中表情
///
/// @param emoticon  选中的表情
/// @param isDeleted 是否删除键，如果是，emoticon == nil
- (void)emoticonCellDidSelectedEmoticon:(RWEmoticon *) emoticon isDeleted:(BOOL)isDeleted;

@end

/// 表情 Cell
@interface RWEmoticonCell : UICollectionViewCell

/// Cell 当前对应数据索引
@property (nonatomic, strong) NSIndexPath *indexPath;

/// 代理
@property (nonatomic, weak) id<RWEmoticonCellDelegate>delegate;

/// 当前 cell 所在 indexPath
//@property (nonatomic) NSIndexPath *indexPath;

/// 当前页面的表情模型数组
@property (nonatomic) NSArray *emoticons;

@end
