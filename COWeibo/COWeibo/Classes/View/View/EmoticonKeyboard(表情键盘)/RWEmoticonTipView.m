//
//  RWEmoticonTipView.m
//  COWeibo
//
//  Created by as888 on 15/12/28.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "RWEmoticonTipView.h"
#import "RWEmoticonButton.h"

@interface RWEmoticonTipView()

@property (nonatomic, strong) RWEmoticonButton *tipButton;

@end

@implementation RWEmoticonTipView

#pragma mark - 设置数据
- (void)setEmoticon:(RWEmoticon *)emoticon {

    if (self.tipButton.emoticon == emoticon) {
        return;
    }

    self.tipButton.emoticon = emoticon;
    
    CGPoint center = self.tipButton.center;
    self.tipButton.center = CGPointMake(center.x, center.y + 16);
    [UIView animateWithDuration:0.25
                          delay:0
         usingSpringWithDamping:0.4
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.tipButton.center = center;
                     }
                     completion:nil];
}

#pragma mark - 构造函数
- (instancetype)init {
    
    self = [super initWithImage:[UIImage imageNamed:@"emoticon_keyboard_magnifier"]];
    if (self) {
        
        _tipButton = [[RWEmoticonButton alloc] init];
        [self addSubview:_tipButton];
                
        // 计算按钮位置
        CGFloat width = 32;
        CGFloat x = (self.bounds.size.width - width) * 0.5;
        CGRect rect = CGRectMake(x, 8, width, width);
   
        _tipButton.frame = rect;
    }
    return self;
}

@end
