//
//  RWStatusPictureCell.h
//  COWeibo
//
//  Created by as888 on 15/12/23.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 配图视图 Cell
@interface RWStatusPictureCell : UICollectionViewCell

///图片 URL
@property (nonatomic,strong) NSURL *imageURL;

@end
