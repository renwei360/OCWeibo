//
//  RWStatusRetweetedView.h
//  COWeibo
//
//  Created by as888 on 15/12/23.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RWStatusViewModel;

@interface RWStatusRetweetedView : UIView

/// 转发微博视图
@property (nonatomic, strong) RWStatusViewModel *viewModel;

@end
