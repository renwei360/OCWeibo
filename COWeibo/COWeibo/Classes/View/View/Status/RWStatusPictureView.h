//
//  RWStatusPictureView.h
//  COWeibo
//
//  Created by as888 on 15/12/23.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 微博配图视图
@interface RWStatusPictureView : UICollectionView

@property (nonatomic, strong) NSArray *urls;

@end
