//
//  RWStatusOriginalView.h
//  COWeibo
//
//  Created by as888 on 15/12/21.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RWStatusViewModel;

/// 原创微博视图
@interface RWStatusOriginalView : UIView

@property (nonatomic, strong) RWStatusViewModel *viewModel;

@end
