//
//  RWStatusToolBar.h
//  COWeibo
//
//  Created by as888 on 15/12/22.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RWStatusViewModel;

@interface RWStatusToolBar : UIView

/// 微博数据模型
@property (nonatomic, strong) RWStatusViewModel *viewModel;

@end
