//
//  RWStatusCellCommon.h
//  COWeibo
//
//  Created by as888 on 15/12/22.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "RWStatusViewModel.h"
#import <sdwebimage/UIImageView+WebCache.h>

/// 微博 Cell 常量定义
/// 控件间距
#define kStatusCellMargin       10
/// 头像宽度
#define kStatusCellIconWidth    35
