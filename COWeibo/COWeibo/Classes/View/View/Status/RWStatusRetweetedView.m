//
//  RWStatusRetweetedView.m
//  COWeibo
//
//  Created by as888 on 15/12/23.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "RWStatusRetweetedView.h"
#import "RWStatusCellCommon.h"
#import "RWStatusPictureView.h"

@implementation RWStatusRetweetedView {

    /// 转发标签
    UILabel *_contentLabel;
    
    /// 配图视图
    RWStatusPictureView *_pictureView;
    
    /// 配图视图的底部约束
    MASConstraint *_pictureBottomConstraint;
}

#pragma mark - 设置数据
- (void)setViewModel:(RWStatusViewModel *)viewModel {

    _viewModel = viewModel;
    
    // 1. 转发微博文字
    _contentLabel.text = viewModel.retweetedText;
    
    // 2. 设置配图视图的内容
    [self setupPictureViewWithURLs:viewModel.status.retweeted_status.pic_urls];
}

/// 更新配图视图约束，
/// - 如果有配图，让当前视图参照 配图视图 设置底部约束
/// - 如果没有配图，让当前视图参照 转发标签 设置底部约束
- (void)setupPictureViewWithURLs:(NSArray *)urls {
    
    // 1. 设置配图视图的数据
    _pictureView.urls = urls;
    
    // 2. 是否隐藏视图
    BOOL hasPicture = urls.class > 0;
    _pictureView.hidden = !hasPicture;
    
    // 3. 判断是否有配图
    [_pictureBottomConstraint uninstall];
    if (hasPicture) {
        
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            _pictureBottomConstraint = make.bottom.equalTo(_pictureView).offset(kStatusCellMargin);
        }];
        
    } else {
    
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            _pictureBottomConstraint = make.bottom.equalTo(_contentLabel).offset(kStatusCellMargin);
        }];
    }
}

#pragma mark - 构造函数
- (instancetype)initWithFrame:(CGRect)frame {

    self = [super initWithFrame:frame];
    if (self) {
        // self.backgroundColor = [UIColor blackColor];
   
        [self setupUI];
    }
    return self;
}

#pragma mark - 设置界面
- (void)setupUI {

    self.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];

    // 1. 创建控件
    _contentLabel = [UILabel ff_lableWithTitle:@"retweeted retweeted retweeted retweeted retweeted retweeted retweeted" color:[UIColor darkGrayColor] fontSize:15 alignment:NSTextAlignmentLeft];
    _pictureView = [[RWStatusPictureView alloc] init];
    
    // 设置配图视图的背景颜色和当前的视图相同
    _pictureView.backgroundColor = self.backgroundColor;
    // 2. 添加控件
    [self addSubview:_contentLabel];
    [self addSubview:_pictureView];
    
    // 3. 自动布局
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(kStatusCellMargin);
        make.right.equalTo(self).offset(-kStatusCellMargin);
        make.top.equalTo(self).offset(kStatusCellMargin);
    }];
    [_pictureView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentLabel);
        make.top.equalTo(_contentLabel.mas_bottom).offset(kStatusCellMargin);
        make.size.mas_equalTo(CGSizeMake(90, 90));
        
    }];
    
    // 4. 设置底部视图的约束
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        _pictureBottomConstraint = make.bottom.equalTo(_pictureView).offset(kStatusCellMargin);
    }];
}

@end
