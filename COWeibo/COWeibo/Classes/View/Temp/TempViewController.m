//
//  TempViewController.m
//  COWeibo
//
//  Created by as888 on 15/12/15.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "TempViewController.h"

@interface TempViewController ()

@end

@implementation TempViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = [NSString stringWithFormat:@"第%zd级界面",self.navigationController.childViewControllers.count];
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem ff_barButtonWithTitle:@"PUSH" imageName:nil target:self action:@selector(push)];
}

#pragma mark - 监听方法
- (void)push {

    TempViewController *vc = [[TempViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
