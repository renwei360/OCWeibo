//
//  UILabel+Extension.h
//  COWeibo
//
//  Created by as888 on 15/12/15.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Extension)

/// 创建 UILabel
///
/// @param title     标题
/// @param color     标题颜色
/// @param fontSize  字体大小
///
/// @return UILabel(文本水平居中)
+ (instancetype)ff_lableWithTitle:(NSString *)title color:(UIColor *)color fontSize:(CGFloat)fontSize;

/// 创建 UILabel
///
/// @param title     标题
/// @param color     标题颜色
/// @param fontSize  字体大小
/// @param alignment 对齐方式
///
/// @return UILabel
+ (instancetype)ff_lableWithTitle:(NSString *)title color:(UIColor *)color fontSize:(CGFloat)fontSize alignment:(NSTextAlignment)alignment;

@end
