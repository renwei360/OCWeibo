//
//  UILabel+Extension.m
//  COWeibo
//
//  Created by as888 on 15/12/15.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "UILabel+Extension.h"

@implementation UILabel (Extension)

+ (instancetype)ff_lableWithTitle:(NSString *)title color:(UIColor *)color fontSize:(CGFloat)fontSize {

    return [self ff_lableWithTitle:title color:color fontSize:fontSize alignment:NSTextAlignmentCenter];
}

+ (instancetype)ff_lableWithTitle:(NSString *)title color:(UIColor *)color fontSize:(CGFloat)fontSize alignment:(NSTextAlignment)alignment {

    UILabel *lable = [[UILabel alloc] init];
    
    lable.text = title;
    lable.textColor = color;
    lable.font = [UIFont systemFontOfSize:fontSize];
    lable.numberOfLines = 0;
    lable.textAlignment = alignment;
    
    [lable sizeToFit];
    
    return lable;
}
@end
