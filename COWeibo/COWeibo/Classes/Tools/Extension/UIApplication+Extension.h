//
//  UIApplication+Extension.h
//  COWeibo
//
//  Created by as888 on 15/12/20.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface UIApplication (Extension)

/// 返回应用程序的代理
+ (AppDelegate *)appDelegate;
/// 根视图控制器
+ (UIViewController *)rootViewController;
/// 返回当前设备对应的启动图片
+ (UIImage *)launchImage;

@end
