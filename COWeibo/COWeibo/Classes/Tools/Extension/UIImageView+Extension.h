//
//  UIImageView+Extension.h
//  COWeibo
//
//  Created by as888 on 15/12/15.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Extension)

/// 使用图像名创建图像视图
///
/// @param imageName 图像名称
///
/// @return UIImageView
+ (instancetype)ff_imageViewWithImageName:(NSString *)imageName;
@end
