//
//  UIImage+ScreenShot.h
//  COWeibo
//
//  Created by as888 on 15/12/26.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ScreenShot)

/// 获取屏幕截图
///
/// @return 屏幕截图图像
+ (UIImage *)screenShot;

@end
