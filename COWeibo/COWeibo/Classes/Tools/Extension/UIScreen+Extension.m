//
//  UIScreen+Extension.m
//  COWeibo
//
//  Created by as888 on 15/12/20.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "UIScreen+Extension.h"

@implementation UIScreen (Extension)

+ (CGSize)ff_screenSize {

    return [UIScreen mainScreen].bounds.size;
}

+ (BOOL)ff_isRetina {

    return [UIScreen ff_scale] >= 2;
}

+ (CGFloat)ff_scale {

    return [UIScreen mainScreen].scale;
}

@end
