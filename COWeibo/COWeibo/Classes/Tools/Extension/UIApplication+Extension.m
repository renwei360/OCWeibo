//
//  UIApplication+Extension.m
//  COWeibo
//
//  Created by as888 on 15/12/20.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import "UIApplication+Extension.h"

@implementation UIApplication (Extension)

+ (AppDelegate *)appDelegate {

    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

+ (UIViewController *)rootViewController {

    return [UIApplication appDelegate].window.rootViewController;
}

+ (UIImage *)launchImage {

    UIImage *image = nil;
    NSArray *launchImages = [NSBundle mainBundle].infoDictionary[@"UILaunchImages"];
    for (NSDictionary *dict in launchImages) {
        
        // 1. 将字符串转化成尺寸
        CGSize size = CGSizeFromString(dict[@"UILaunchImageSize"]);
        
        // 2. 与当前屏幕进行比较
        if (CGSizeEqualToSize(size, [UIScreen ff_screenSize])) {
            NSString *filename = dict[@"UILaunchImageName"];
            image = [UIImage imageNamed:filename];
            
            break;
        }
    }
    return image;
}

@end
