//
//  UIScreen+Extension.h
//  COWeibo
//
//  Created by as888 on 15/12/20.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScreen (Extension)

+ (CGSize)ff_screenSize;
+ (BOOL)ff_isRetina;
+ (CGFloat)ff_scale;

@end
