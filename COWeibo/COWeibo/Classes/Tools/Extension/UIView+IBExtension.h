//
//  UIView+IBExtension.h
//  COWeibo
//
//  Created by as888 on 15/12/16.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface UIView (IBExtension)

/// 边线颜色
@property (nonatomic, strong) IBInspectable UIColor *borderColor;
/// 变现宽度
@property (nonatomic, assign) IBInspectable CGFloat borderWidth;
/// 角半径
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

@end
