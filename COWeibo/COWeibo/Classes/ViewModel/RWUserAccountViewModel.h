//
//  RWUserAccountViewModel.h
//  COWeibo
//
//  Created by as888 on 15/12/20.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RWUserAccont.h"
@interface RWUserAccountViewModel : NSObject

/// 用户账户单例
+ (instancetype)sharedUserAccount;

/// 用户账户模型只读属性
@property (nonatomic, strong, readonly) RWUserAccont *userAccount;

/// 用户是否登录
@property (nonatomic,assign,readonly) BOOL isLogon;
/// 用户头像 URL
@property (nonatomic, assign, readonly) NSURL *avatarURL;

/// 发起网路请求，加载 access token
/// token 加载完成后，会再次加载用户信息并保存
///
/// @param code      授权码
/// @param completed 完成回调
- (void)accessTokenWithCode:(NSString *)code completed:(void (^) (bool isSuccessed))completed;

@end
