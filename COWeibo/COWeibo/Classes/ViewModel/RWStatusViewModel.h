//
//  RWStatusViewModel.h
//  COWeibo
//
//  Created by as888 on 15/12/22.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RWStatus.h"

@interface RWStatusViewModel : NSObject
/// 微博模型
@property (nonatomic ,strong) RWStatus *status;
/// 用户头像 URL
@property (nonatomic, strong) NSURL *userAvatarURL;
///  会员图标
@property (nonatomic, strong) UIImage *memberImage;
/// 认证图像
@property (nonatomic, strong) UIImage *verifiedImage;

/// 转发数
@property (nonatomic, strong) NSString *repostsCountStr;
/// 评论数
@property (nonatomic, strong) NSString *commentsCountStr;
/// 表态数
@property (nonatomic, strong) NSString *attitudesCountStr;
/// 是否有转发微博
@property (nonatomic, assign) BOOL hasRetweeted;
/// 转发微博的文本
@property (nonatomic, strong) NSString *retweetedText;
///// 微博配图数组
///// - 如果是原创微博，返回原创微博配图 URL 数组
///// - 如果是转发微博，返回转发微博配图 URL 数组
//@property (nonatomic, readonly) NSArray *pictureUrls;
///// 是否有配图
//@property (nonatomic, readonly) BOOL hasPicture;
/// 使用 微博模型实例化视图模型
///
/// @param status 微博模型对象
///
/// @return 视图模型
+ (instancetype)viewModelWithStatus:(RWStatus *)status;

@end
