//
//  AppDelegate.h
//  COWeibo
//
//  Created by as888 on 15/12/14.
//  Copyright © 2015年 任伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

/// 切换控制器通知
extern NSString *const RWSwitchRootViewControllerNotification;